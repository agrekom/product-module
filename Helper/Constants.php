<?php

namespace Agrekom\Product\Helper;

class Constants
{

    const PRODUCT_ADDITIONAL_TAX_INFO_SYSTEM_PATH = 'agrekom_product/additional/tax_info';
    const PRODUCT_CATEGORY_ADDITIONAL_INFO_SYSTEM_PATH = 'agrekom_product/category/additional_info';
    const PRODUCT_PRICE_ADDITIONAL_INFO_SYSTEM_PATH = 'agrekom_product/price/additional_info';

}
