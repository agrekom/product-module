<?php

namespace Agrekom\Product\Helper;

class SystemConfiguration extends \Magento\Framework\App\Helper\AbstractHelper
{

    public function getProductPriceAdditionalInfo(): string
    {
        return (string) $this->scopeConfig->getValue(
            \Agrekom\Product\Helper\Constants::PRODUCT_PRICE_ADDITIONAL_INFO_SYSTEM_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getProductCategoryAdditionalInfo(): string
    {
        return (string) $this->scopeConfig->getValue(
            \Agrekom\Product\Helper\Constants::PRODUCT_CATEGORY_ADDITIONAL_INFO_SYSTEM_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getProductAdditionalTaxInfo(): string
    {
        return (string) $this->scopeConfig->getValue(
            \Agrekom\Product\Helper\Constants::PRODUCT_ADDITIONAL_TAX_INFO_SYSTEM_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

}
